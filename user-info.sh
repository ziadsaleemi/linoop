#!/bin/bash

# How to run a command and show its value in the shell/ on the terminal
# echo $(echo $SHELL)
read -p "Enter your username :" username
myid=$(id -u)
mygid=$(id -g)
echo "My username is $username and id is $myid and my gid is $mygid"

echo "$username $myuid $mygid" > output.txt
