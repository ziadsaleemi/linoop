#!/bin/bash
# Asking for users input
read -p "Enter the username: " user

# Adding a user 
echo "Creating $user"
sleep 2
adduser $user

# Making sure user exists
sleep 2
echo $(id $user)

read -p "Enter the name of dir1: " dir1
read -p "Enter the name of dir2: " dir2

echo "Creating 1: $dir1 and 2: $dir2 directories"
sleep 2
mkdir -p /etc/httpd/$dir1
mkdir -p /etc/httpd/$dir2

echo "Checking if the directories exist"
sleep 2
echo $(ls -ld /etc/httpd/$dir1)
echo $(ls -ld /etc/httpd/$dir2)

echo "Changing the ownership of $dir1 $dir2 to $user"
sleep 2
echo $(chown $user:$user /etc/httpd/$dir1)
echo $(chown $user:$user /etc/httpd/$dir2)

echo "Checking if the ownership is changed to $user"
sleep 2
echo $(ls -ld /etc/httpd/$dir1)
echo $(ls -ld /etc/httpd/$dir2)

