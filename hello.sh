#!/bin/bash

echo "Hello World"

echo "Creating Directories"

# Creating directories using mkdir command and using -p flag so we don't get any errors if the directories already exist

mkdir -p linoop/students/Majid
mkdir -p linoop/students/Atif
mkdir -p linoop/students/Halima
mkdir -p linoop/students/Shaheera
mkdir -p linoop/students/Areesha
mkdir -p linoop/students/Sehrash

tree linoop

echo "Deleting the directories"
rm -rf linoop
