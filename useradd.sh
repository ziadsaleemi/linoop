#!/bin/bash


usrid=`awk -F ":" '{print $3}' /etc/passwd`
usrname=`awk -F ":" '{print $1}' /etc/passwd`

read -p "Username: " username
for i in $usrname; do
	if [[ $i == $username ]]; then
		echo "User $username already exists";
		exit
	fi
done

read -p "User ID : " userid

for i in $usrid; do
	if [[ $i == $userid ]]; then
		echo "User id $userid already exists";
		exit 1
	fi
done
read -p "Password: " password
read -p "Confirm Password: " conf_password
if [[ $password != $conf_password ]]; then
	echo "Password does not match"
fi

