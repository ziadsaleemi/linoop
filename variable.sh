#!/bin/bash

echo "### Setting up the variable human = alien ###"
sleep 3

human="alien"

echo " This is a $human"

echo " The $human lives in NY"

echo " The $human drives a car"
echo "  "
echo " ### Changing the variable value to human ###"
human="human"
sleep 3
echo " The $human doesn't have a driving license"


echo " The $human get tickets every other day"

echo $human
