#!/bin/bash
#set -x
value1="Hello"
value2="World"

# Printing Hello World on the shell
echo "$value1 $value2"

# Echoing another line
# value1="Bye"
# value2="Mars"
echo "$value1 $value2"
